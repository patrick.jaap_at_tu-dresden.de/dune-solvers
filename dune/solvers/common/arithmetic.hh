// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef ARITHMETIC_HH
#define ARITHMETIC_HH

#warning arithmetic.hh is deprecated, please use dune-matrix-vector instead!

#include <dune/matrix-vector/axpy.hh>
#include <dune/matrix-vector/axy.hh>
#include <dune/matrix-vector/crossproduct.hh>
#include <dune/matrix-vector/matrixtraits.hh>
#include <dune/matrix-vector/promote.hh>
#include <dune/matrix-vector/scalartraits.hh>
#include <dune/matrix-vector/transpose.hh>

namespace Arithmetic
{
    using Dune::MatrixVector::Promote;
    using Dune::MatrixVector::ScalarTraits;
    using Dune::MatrixVector::MatrixTraits;
    using Dune::MatrixVector::Transposed;
    using Dune::MatrixVector::addProduct;
    using Dune::MatrixVector::subtractProduct;
    using Dune::MatrixVector::crossProduct;
    using Dune::MatrixVector::transpose;
    using Dune::MatrixVector::Axy;
    using Dune::MatrixVector::bmAxy;
}

#endif
