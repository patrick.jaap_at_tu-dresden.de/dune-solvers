install(FILES
    compressedmultigridtransfer.hh
    densemultigridtransfer.hh
    genericmultigridtransfer.hh
    mandelobsrestrictor.cc
    mandelobsrestrictor.hh
    multigridtransfer.hh
    obstaclerestrictor.hh
    truncatedcompressedmgtransfer.cc
    truncatedcompressedmgtransfer.hh
    truncateddensemgtransfer.cc
    truncateddensemgtransfer.hh
    truncatedmgtransfer.hh
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/solvers/transferoperators)
