// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <config.h>

#include <array>
#include <iostream>
#include <sstream>

// dune-common includes
#include <dune/common/bitsetvector.hh>
#include <dune/common/parallel/mpihelper.hh>

// dune-istl includes
#include <dune/istl/bcrsmatrix.hh>

// dune-grid includes
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

// dune-solver includes
#include <dune/solvers/iterationsteps/obstacletnnmgstep.hh>
#include <dune/solvers/transferoperators/compressedmultigridtransfer.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/solvers/loopsolver.hh>


#include "common.hh"

template<class GridType, class MatrixType, class VectorType>
void solveObstacleProblemByTNNMG(const GridType& grid, const MatrixType& mat, VectorType& x, const VectorType& rhs, int maxIterations=100, double tolerance=1.0e-10)
{
    // bit vector marking dofs to ignore
    typedef typename ObstacleTNNMGStep<MatrixType, VectorType>::BitVector BitVector;
    BitVector ignore(rhs.size());
    ignore.unsetAll();

    solveObstacleProblemByTNNMG(grid, mat, x, rhs, ignore, maxIterations, tolerance);
}


template<class GridType, class MatrixType, class VectorType, class BitVector>
void solveObstacleProblemByTNNMG(const GridType& grid, const MatrixType& mat, VectorType& x, const VectorType& rhs, const BitVector& ignore, int maxIterations=100, double tolerance=1.0e-10)
{
    typedef VectorType Vector;
    typedef MatrixType Matrix;
    typedef EnergyNorm<Matrix, Vector> Norm;
    typedef ::LoopSolver<Vector> Solver;
    typedef ObstacleTNNMGStep<Matrix, Vector> TNNMGStep;
    typedef typename TNNMGStep::Transfer Transfer;
    typedef typename TNNMGStep::BoxConstraintVector BoxConstraintVector;
    typedef CompressedMultigridTransfer<Vector, BitVector, Matrix> TransferImplementation;

    const int blockSize = VectorType::block_type::dimension;

    std::vector<std::shared_ptr<Transfer>> transfer(grid.maxLevel());
    for (size_t i = 0; i < transfer.size(); ++i)
    {
        // create transfer operators from level i to i+1
        auto t = std::make_shared<TransferImplementation>();
        t->setup(grid, i, i+1);
        transfer[i] = t;
    }

    // create double obstacle constraints
    BoxConstraintVector boxConstraints(rhs.size());
    for (size_t i = 0; i < boxConstraints.size(); ++i)
        for (int j = 0; j < blockSize; ++j)
            boxConstraints[i][j] = { -1, +1 };

    // we want to control errors with respect to the energy norm induced by the matrix
    Norm norm(mat);

    // create iteration step
    TNNMGStep tnnmgStep(mat, x, rhs, ignore, boxConstraints, transfer);
    tnnmgStep.preprocess();
    tnnmgStep.nestedIteration();

    // cretae loop solver
    Solver solver(tnnmgStep, maxIterations, tolerance, norm, Solver::FULL);

    // solve problem
    solver.solve();
}





template <class GridType>
bool checkWithGrid(const GridType& grid, const std::string fileName="")
{
    bool passed = true;

    static const int dim = GridType::dimension;

    typedef typename Dune::FieldMatrix<double, 1, 1> MatrixBlock;
    typedef typename Dune::FieldVector<double, 1> VectorBlock;
    typedef typename Dune::BCRSMatrix<MatrixBlock> Matrix;
    typedef typename Dune::BlockVector<VectorBlock> Vector;
    typedef typename Dune::BitSetVector<1> BitVector;

    typedef typename GridType::LeafGridView GridView;
    typedef typename Dune::FieldVector<double, dim> DomainType;
    typedef typename Dune::FieldVector<double, 1> RangeType;


    const GridView gridView = grid.leafGridView();

    Matrix A;
    constructPQ1Pattern(gridView, A);
    A=0.0;
    assemblePQ1Stiffness(gridView, A);

    Vector rhs(A.N());
    rhs = 0;
    ConstantFunction<DomainType, RangeType> f(50);
    assemblePQ1RHS(gridView, rhs, f);

    Vector u(A.N());
    u = 0;

    BitVector ignore(A.N());
    ignore.unsetAll();
    markBoundaryDOFs(gridView, ignore);


    solveObstacleProblemByTNNMG(grid, A, u, rhs, ignore);

    if (fileName!="")
    {
        typename Dune::VTKWriter<GridView> vtkWriter(gridView);
        vtkWriter.addVertexData(u, "solution");
        vtkWriter.write(fileName);
    }


    return passed;
}


template <int dim>
bool checkWithYaspGrid(int refine, const std::string fileName="")
{
    bool passed = true;

    typedef Dune::YaspGrid<dim> GridType;

    typename Dune::FieldVector<typename GridType::ctype,dim> L(1.0);
    typename std::array<int,dim> s;
    std::fill(s.begin(), s.end(), 1);

    GridType grid(L, s);

    for (int i = 0; i < refine; ++i)
        grid.globalRefine(1);

    std::cout << "Testing with YaspGrid<" << dim << ">" << std::endl;
    std::cout << "Number of levels: " << (grid.maxLevel()+1) << std::endl;
    std::cout << "Number of leaf nodes: " << grid.leafGridView().size(dim) << std::endl;

    passed = passed and checkWithGrid(grid, fileName);


    return passed;
}




int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);
    bool passed(true);


    int refine1d = 10;
    int refine2d = 5;
    int refine3d = 3;

    if (argc>1)
        std::istringstream(argv[1]) >> refine1d;
    if (argc>2)
        std::istringstream(argv[2]) >> refine2d;
    if (argc>3)
        std::istringstream(argv[3]) >> refine3d;

    passed = passed and checkWithYaspGrid<1>(refine1d, "obstacletnnmgtest_yasp_1d_solution");
    passed = passed and checkWithYaspGrid<2>(refine2d, "obstacletnnmgtest_yasp_2d_solution");
    passed = passed and checkWithYaspGrid<3>(refine3d, "obstacletnnmgtest_yasp_3d_solution");

    return passed ? 0 : 1;
}
