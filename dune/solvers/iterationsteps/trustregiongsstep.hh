// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_TRUST_REGION_BLOCK_GAUSS_SEIDEL_STEP_HH
#define DUNE_TRUST_REGION_BLOCK_GAUSS_SEIDEL_STEP_HH

/** \file
    \brief A Gauss-Seidel iterator which does not assume that the functional
    is convex, but which expects a bounded admissible set.
*/

#include "projectedblockgsstep.hh"
#include <dune/solvers/common/boxconstraint.hh>

    /** \brief A Gauss-Seidel iterator which does not assume that the functional
        is convex, but which expects a bounded admissible set.
    */
    template<class MatrixType, class VectorType>
    class TrustRegionGSStep : public ProjectedBlockGSStep<MatrixType, VectorType>
    {
        using VectorBlock = typename VectorType::block_type;
        using Field = typename VectorType::field_type;

        enum {BlockSize = VectorBlock::dimension};

    public:
        //! Default constructor.  Doesn't init anything
        TrustRegionGSStep() {}

        //! Constructor with a linear problem
        TrustRegionGSStep(MatrixType& mat, VectorType& x, VectorType& rhs)
            : LinearIterationStep<MatrixType,VectorType>(mat, x, rhs)
        {}

        //! Perform one iteration
        virtual void iterate();
    };

#include "trustregiongsstep.cc"

#endif
