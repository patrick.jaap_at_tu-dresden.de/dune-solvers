// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef ISTL_SEQILU0_STEP_HH
#define ISTL_SEQILU0_STEP_HH

/** \file
    \brief A wrapper class for the ISTL SeqILU0 implementation
 */

#include <memory>

#include <dune/solvers/iterationsteps/lineariterationstep.hh>

#include <dune/istl/preconditioners.hh>

/** \brief A wrapper class for the ISTL SeqILU0 implementation
 */
template <class MatrixType, class VectorType>
class ISTLSeqILU0Step
    // FIXME: ignoreNodes are not handled
    : public LinearIterationStep<MatrixType, VectorType>
{
    typedef Dune::SeqILU0<MatrixType,VectorType,VectorType> SeqILU0;

public:
    ISTLSeqILU0Step (double relaxationFactor)
        : relaxationFactor_(relaxationFactor)
    {}

    /** \brief Sets up an algebraic hierarchy
     */
    virtual void preprocess() override {
        if (needReconstruction_) {
            seqILU0_ = std::make_unique<SeqILU0>(*this->mat_, relaxationFactor_);
            needReconstruction_ = false;
        }
        // Note: as of now, pre() is a dummy
        mutable_rhs = *this->rhs_;
        seqILU0_->pre(*this->x_,mutable_rhs);
    }

    /** \brief Perform one iteration */
    virtual void iterate() override {
        mutable_rhs = *this->rhs_;
        seqILU0_->apply(*this->x_, mutable_rhs);
    }

    virtual void setMatrix(const MatrixType& mat) override {
        this->mat_ = Dune::stackobject_to_shared_ptr(mat);
        needReconstruction_ = true;
    }

private:

    /** \brief The dune-istl sequential ILU0 preconditioner */
    std::unique_ptr<SeqILU0> seqILU0_;

    VectorType mutable_rhs;

    double const relaxationFactor_;

    /** \brief Calling the Dune::SeqILU0 constructor is expensive; we
               thus remember if setMatrix has been called. And only
               call the constructor if necessary */
    bool needReconstruction_;
};
#endif
