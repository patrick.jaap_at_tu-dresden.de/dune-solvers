// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#include <dune/common/bitsetvector.hh>
#include <dune/common/timer.hh>

#include <dune/istl/io.hh>

// Using a monotone multigrid as the inner solver
#include <dune/solvers/iterationsteps/mmgstep.hh>
#include <dune/solvers/solvers/loopsolver.hh>

template <class ProblemType, class VectorType, class MatrixType>
void TrustRegionSolver<ProblemType,VectorType,MatrixType>::solve()
{
    MonotoneMGStep<MatrixType,VectorType>* mgStep = nullptr;

    // if the inner solver is a monotone multigrid set up a max-norm trust-region
    if (dynamic_cast<::LoopSolver<VectorType>*>(innerSolver_.get())) {
        mgStep = dynamic_cast<MonotoneMGStep<MatrixType,VectorType>*>(&dynamic_cast<::LoopSolver<VectorType>*>(innerSolver_.get())->getIterationStep());
    }

    // setup the trust-region in the maximums norm
    std::vector<BoxConstraint<field_type,blocksize> > trustRegionObstacles(x_.size());
    for (size_t i=0; i<trustRegionObstacles.size(); i++)
        for (int j=0; j<blocksize; j++)
            trustRegionObstacles[i][j] = { -initialRadius_, initialRadius_ };

    mgStep->setObstacles(trustRegionObstacles);

    // /////////////////////////////////////////////////////
    //   Trust-Region Solver
    // /////////////////////////////////////////////////////

    double oldEnergy = problem_->energy(x_);

    for (int i=0; i<this->maxIterations_; i++) {

        Dune::Timer totalTimer;
        if (this->verbosity_ == Solver::FULL) {
            std::cout << "----------------------------------------------------" << std::endl;
            std::cout << "      Trust-Region Step Number: " << i
                      << ",     radius: " << initialRadius_
                      << ",     energy: " << oldEnergy << std::endl;
            std::cout << "----------------------------------------------------" << std::endl;
        }

        // Assemble the quadratic problem
        Dune::Timer gradientTimer;
        problem_->assembleQP(x_);

        // Compute gradient norm to monitor convergence
        VectorType gradient = problem_->f_;
        for (size_t j=0; j<gradient.size(); j++)
            for (size_t k=0; k<gradient[j].size(); k++)
                if (mgStep->ignore()[j][k])
                    gradient[j][k] = 0;

        if (this->verbosity_ == Solver::FULL)
            std::cout << "Gradient norm: " << this->errorNorm_->operator()(gradient) << std::endl;

        if (this->verbosity_ == Solver::FULL)
            std::cout << "Assembly took " << gradientTimer.elapsed() << " sec." << std::endl;

        VectorType corr(x_.size());
        corr = 0;

        mgStep->setProblem(problem_->A_, corr, problem_->f_);

        innerSolver_->preprocess();

        ///////////////////////////////
        //    Solve !
        ///////////////////////////////

        // Solve the subproblem until we find an acceptable iterate
        if (this->verbosity_ == NumProc::FULL)
            std::cout << "Solve quadratic problem..." << std::endl;
        while(true) {

            Dune::Timer solutionTimer;
            innerSolver_->solve();
            if (this->verbosity_ == NumProc::FULL)
                std::cout << "Solving the quadratic problem took " << solutionTimer.elapsed() << " seconds." << std::endl;

            corr = mgStep->getSol();

            if (this->verbosity_ == NumProc::FULL)
                std::cout << "Infinity norm of the correction: " << corr.infinity_norm() << std::endl;

            if (corr.infinity_norm() < this->tolerance_) {
                if (this->verbosity_ != NumProc::QUIET)
                    std::cout << i+1 << " trust-region steps were taken." << std::endl;
                return;
            }

            // ////////////////////////////////////////////////////
            //   Check whether trust-region step can be accepted
            // ////////////////////////////////////////////////////

            VectorType newIterate = x_;
            newIterate += corr;
            if (this->verbosity_ == NumProc::FULL)
                std::cout << "Infinity norm of new iterate : " << newIterate.infinity_norm() << std::endl;

            field_type energy    = problem_->energy(newIterate);

            // compute the model decrease
            field_type modelDecrease = problem_->modelDecrease(corr);

            field_type relativeModelDecrease = modelDecrease / std::fabs(energy);

            if (this->verbosity_ == NumProc::FULL) {
                std::cout << "Absolute model decrease: " << modelDecrease
                    << ",  functional decrease: " << oldEnergy - energy << std::endl;
                std::cout << "Relative model decrease: " << relativeModelDecrease
                    << ",  functional decrease: " << (oldEnergy - energy)/energy << std::endl;
            }

            // assure that the inner solver works correctly
            assert(modelDecrease >= 0);

            if (energy >= oldEnergy) {
                if (this->verbosity_ == NumProc::FULL)
                    printf("No energy descent!\n");
            }

            if (energy >= oldEnergy &&
                    (std::abs((oldEnergy-energy)/energy) < 1e-9 || relativeModelDecrease < 1e-9)) {
                if (this->verbosity_ == NumProc::FULL)
                    std::cout << "Suspecting rounding problems, stopping here!" << std::endl;

                if (this->verbosity_ != NumProc::QUIET)
                    std::cout << i+1 << " trust-region steps were taken." << std::endl;

                x_ = newIterate;
                return;
            }

            // //////////////////////////////////////////////
            //   Check for acceptance of the step
            // //////////////////////////////////////////////
            if ( (oldEnergy-energy) / modelDecrease > 0.9) {
                // very successful iteration

                x_ = newIterate;
                scaleTrustRegionRadius(trustRegionObstacles,enlargeRadius_);
                std::cout << "Very Successful iteration, enlarging radius! " <<initialRadius_<<std::endl;

                // current energy becomes 'oldEnergy' for the next iteration
                oldEnergy = energy;
                break;

            } else if ( (oldEnergy-energy) / modelDecrease > 0.01
                    || std::abs(oldEnergy-energy) < 1e-12) {
                // successful iteration
                x_ = newIterate;

                // current energy becomes 'oldEnergy' for the next iteration
                oldEnergy = energy;
                break;

            } else {

                // unsuccessful iteration

                // Decrease the trust-region radius
                scaleTrustRegionRadius(trustRegionObstacles,shrinkRadius_);

                if (this->verbosity_ == NumProc::FULL)
                    std::cout << "Unsuccessful iteration, shrinking radius! " <<initialRadius_<<std::endl;
            }
        }
        std::cout << "iteration took " << totalTimer.elapsed() << " sec." << std::endl;
    }

}
