// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_COMMON_BOX_CONSTRAINT_HH
#define DUNE_SOLVERS_COMMON_BOX_CONSTRAINT_HH

#include <array>
#include <limits>

#include <dune/common/fvector.hh>

#include "interval.hh"

template <class T, int dim>
class BoxConstraint {
public:
    using Interval = typename Dune::Solvers::Interval<T>;

    /** \brief Default constructor, makes the obstacle as large as possible */
    BoxConstraint() {
        clear();
    }

    /** \brief Set obstacle values to +/- std::numeric_limits<T>::max() */
    void clear() {
        for (int i=0; i<dim; i++)
            val[i] = { -std::numeric_limits<T>::max(),
                       std::numeric_limits<T>::max() };
    }

    //! Subtract vector from box
    BoxConstraint<T,dim>& operator-= (const Dune::FieldVector<T, dim>& v)
    {
        for (int i=0; i<dim; i++) {
            val[i][0] -= v[i];
            val[i][1] -= v[i];
        }
        return *this;
    }

    Interval const &operator[](size_t i) const {
        return val[i];
    }
    Interval &operator[](size_t i) {
        return val[i];
    }

    //! Access to the lower obstacles
    T& lower(size_t i) {return val[i][0];}

    //! Const access to the lower obstacles
    const T& lower(size_t i) const {return val[i][0];}

    //! Access to the upper obstacles
    T& upper(size_t i) {return val[i][1];}

    //! Const access to the upper obstacles
    const T& upper(size_t i) const {return val[i][1];}

    //! Send box constraint to an output stream
    friend
    std::ostream& operator<< (std::ostream& s, const BoxConstraint<T,dim>& v)
    {
        for (int i=0; i<dim; i++)
            s << "Direction: " << i <<",  val[0] = " << v[i][0]
              << ", val[1] = " << v[i][1] << std::endl;

        return s;
    }

protected:
    std::array<Interval, dim> val;
};

#endif
