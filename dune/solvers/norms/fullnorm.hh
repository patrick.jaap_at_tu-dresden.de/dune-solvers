// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef FULLNORM_HH
#define FULLNORM_HH

#include <cmath>

#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include "norm.hh"

/** \brief Vector norm induced by dense low rank linear operator M
 *
 *  \f$M = m^t*m\f$
 *  \f$\Vert u \Vert_M = (u, Mu)^{1/2} = (mu,mu)^{1/2}\f$
 *
 * \tparam LowRankFactor the type of the factor used to represent the low rank operator
 * \tparam VectorType the vector type the norm may be applied to
 */
template <class LowRankFactor=Dune::BlockVector<Dune::FieldVector <double,1> >, class V=Dune::BlockVector<Dune::FieldVector <double,1> > >
class FullNorm: public Norm<V>
{
    public:
        typedef V VectorType;
        using Base = Norm<V>;

        /** \brief The type used for the result */
        using typename Base::field_type;

        FullNorm(const field_type alpha, const LowRankFactor &lowRankFactor) :
            lowRankFactor_(lowRankFactor),
            alpha(alpha)
        {}

        //! Compute the norm of the given vector
        field_type operator()(const VectorType &f) const override
        {
            return std::sqrt(normSquared(f));
        }

        //! Compute the square of the norm of the given vector
        virtual field_type normSquared(const VectorType& f) const override
        {
            VectorType r(lowRankFactor_.N());
            r = 0.0;

            lowRankFactor_.umv(f,r);

            return std::abs(alpha*(r*r));
        }

        //! Compute the norm of the difference of two vectors
        field_type diff(const VectorType &f1, const VectorType &f2) const override
        {
            VectorType r(lowRankFactor_.N());
            r = 0.0;

            for (typename LowRankFactor::size_type k=0; k<lowRankFactor_.N(); ++k)
                for (typename LowRankFactor::size_type i=0; i<lowRankFactor_.M(); ++i)
                    lowRankFactor_[k][i].umv(f1[i]-f2[i],r[k]);

            return std::sqrt(std::abs(alpha*(r*r)));
        }

    private:
        const LowRankFactor& lowRankFactor_;

        const field_type alpha;

};

template<>
class FullNorm<Dune::BlockVector<Dune::FieldVector<double,1> >, Dune::BlockVector<Dune::FieldVector<double,1> > >:
    public Norm<Dune::BlockVector<Dune::FieldVector<double,1> > >
{
    public:
        typedef Dune::BlockVector<Dune::FieldVector<double,1> > VectorType;
        using Base = Norm<VectorType>;

        /** \brief The type used for the result */
        using typename Base::field_type;
    private:
        typedef VectorType::size_type SizeType;
    public:

        FullNorm(const field_type alpha, const VectorType &m) :
            m(m),
            alpha(alpha)
        {}

        //! Compute the norm of the given vector
        field_type operator()(const VectorType &f) const override
        {
            return std::sqrt(normSquared(f));
        }

        //! Compute the square of the norm of the given vector
        virtual field_type normSquared(const VectorType& f) const override
        {
            field_type r = 0.0;

            for (SizeType row = 0; row < f.size(); ++row)
                r += m[row] * f[row];

            return std::abs(alpha*r*r);
        }

        //! Compute the norm of the difference of two vectors
        field_type diff(const VectorType &f1, const VectorType &f2) const override
        {
            field_type r = 0.0;

            for (SizeType row = 0; row < f1.size(); ++row)
                r += (field_type)m[row] * (f1[row] - f2[row]);

            return std::sqrt(std::abs(alpha*r*r));
        }

    private:
        const VectorType &m;

        const field_type alpha;

};
#endif
