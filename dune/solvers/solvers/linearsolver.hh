// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_SOLVERS_LINEARSOLVER_HH
#define DUNE_SOLVERS_SOLVERS_LINEARSOLVER_HH

#include <dune/solvers/solvers/solver.hh>

namespace Dune {

namespace Solvers {

/** \brief Abstract base class for solvers that solve linear problems
 *
 * Linear problems are problems that involve a fixed matrix and a right-hand-side
 * vector.  Additional constraints are allowed.
 */
template <class Matrix, class Vector>
class LinearSolver : public Solver
{
public:
  /** \brief Constructor taking all relevant data */
  LinearSolver(VerbosityMode verbosity)
  : Solver(verbosity)
  {}

  /** \brief Set up the linear problem to solve */
  virtual void setProblem(const Matrix& matrix,
                          Vector& x,
                          const Vector& rhs) = 0;

};

}   // namespace Solvers

}   // namespace Dune

#endif
