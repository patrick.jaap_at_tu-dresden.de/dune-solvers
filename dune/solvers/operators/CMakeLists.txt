install(FILES
    lowrankoperator.hh
    nulloperator.hh
    sumoperator.hh
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/solvers/operators)
