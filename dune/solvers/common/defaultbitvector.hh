// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=8 sw=2 sts=2:
#ifndef DUNE_SOLVERS_COMMON_DEFAULTBITVECTOR_HH
#define DUNE_SOLVERS_COMMON_DEFAULTBITVECTOR_HH

#include <dune/common/bitsetvector.hh>
#include <dune/common/fvector.hh>
#include <dune/common/typetraits.hh>

#include <dune/istl/bvector.hh>
#include <dune/istl/multitypeblockvector.hh>

#include <dune/solvers/common/tuplevector.hh>

namespace Dune {
namespace Solvers {


namespace Imp {

template<class Vector>
struct DefaultBitVector
{
  static_assert(AlwaysFalse<Vector>::value, "No DefaultBitVector known for this type.");
  using type = void;
};

template<class T, int i>
struct DefaultBitVector<FieldVector<T,i>>
{
  using type = std::bitset<i>;
};

template<class T, class A>
struct DefaultBitVector<BlockVector<T,A>>
{
  using type = typename std::vector<typename DefaultBitVector<T>::type>;
};

template<class T, class A>
struct DefaultBitVector<std::vector<T,A>>
{
  using type = typename std::vector<typename DefaultBitVector<T>::type>;
};

template<class T, int i, class A>
struct DefaultBitVector<BlockVector<FieldVector<T,i>, A>>
{
  using type = BitSetVector<i>;
};

template<typename... Args>
struct DefaultBitVector<MultiTypeBlockVector<Args...> >
{
  using type = TupleVector<typename DefaultBitVector<Args>::type...>;
};

} // end namespace Imp



/**
 * \brief Type alias for a bit vector that fits to T
 *
 * \tparam T A nested container type
 *
 * This alias provides a default choice of a bit vector
 * that has the same nested structure as T.
 */
template<class T>
using DefaultBitVector_t = typename Imp::DefaultBitVector<T>::type;



} // end namespace Solvers
} // end namespace Dune



#endif // DUNE_SOLVERS_COMMON_DEFAULTBITVECTOR_HH

