// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <cmath>
#include <limits>
#include <iomanip>

#include <dune/solvers/norms/energynorm.hh>

template <class MatrixType, class VectorType>
void TruncatedCGSolver<MatrixType, VectorType>::check() const
{

    if (preconditioner_)
        preconditioner_->check();

}

template <class MatrixType, class VectorType>
void TruncatedCGSolver<MatrixType, VectorType>::solve()
{

    int i;

    VectorType& b = *rhs_;

    // Check whether the solver is set up properly
    check();

    // set up preconditioner
    if (preconditioner_)
        preconditioner_->preprocess();

    if (this->verbosity_ != NumProc::QUIET)
        std::cout << "--- Truncated CGSolver ---\n";

    if (this->verbosity_ == NumProc::FULL)
    {
        std::cout << " iter";
        std::cout << "          error";
        std::cout << "     rate";
        std::string header;
        if (preconditioner_) {
            header = preconditioner_->getOutput();
            std::cout << header;
        }
        std::cout << std::endl;
        std::cout << "-----";
        std::cout << "---------------";
        std::cout << "---------";
        for(size_t i=0; i<header.size(); ++i)
            std::cout << "-";
        std::cout << std::endl;
    }

    field_type error = std::numeric_limits<field_type>::max();

    field_type normOfOldCorrection = 0;
    field_type totalConvRate = 1;
    this->maxTotalConvRate_ = 0;
    int convRateCounter = 0;

    // overwrite b with defect
    matrix_->mmv(*x_,b);

    VectorType p(*x_);              // the search direction
    VectorType q(*x_);              // a temporary vector

    // some local variables
    field_type rho,rholast,beta;

    // determine initial search direction
    p = 0;                          // clear correction

    // apply preconditioner
    if (preconditioner_) {
        preconditioner_->setProblem(*matrix_,p,b);
        preconditioner_->iterate();
        p = preconditioner_->getSol();
    } else
        p = b;

    rholast = p*b;         // orthogonalization

    field_type solutionNormSquared = 0;
    field_type solutionTimesCorrection = 0;
    field_type correctionNormSquared = rholast;

    // Loop until desired tolerance or maximum number of iterations is reached
    for (i=0; i<this->maxIterations_ && (error>this->tolerance_ || std::isnan(error)); i++) {

        // Backup of the current solution for the error computation later on
        VectorType oldSolution = *x_;

        // ///////////////////////////////////////////////////////////////////////
        //   Perform one iteration step.  The current search direction is in p
        // ///////////////////////////////////////////////////////////////////////

        field_type energyNormSquared = EnergyNorm<MatrixType,VectorType>::normSquared(p, *matrix_);

#if 0  // I used this for debugging
        std::cout << "Computed values: " << std::endl;
        std::cout << "   " << trustRegionScalarProduct(p,p)
                  << "   " << trustRegionScalarProduct(*x_,p)
                  << "   " << trustRegionScalarProduct(*x_,*x_) << std::endl;

        std::cout << "Recursive values: " << std::endl;
        std::cout << "   " << correctionNormSquared
                  << "   " << solutionTimesCorrection
                  << "   " << solutionNormSquared
                  << std::endl;
#endif

        if (preconditioner_==NULL) {

            correctionNormSquared = trustRegionScalarProduct(p,p);
            solutionTimesCorrection = trustRegionScalarProduct(*x_,p);
            solutionNormSquared = trustRegionScalarProduct(*x_,*x_);

        }

        if (correctionNormSquared <= 0) {

            if (this->historyBuffer_!="")
                this->writeIterate(*x_, i);
            return;

        }

        // if energy is concave in the search direction go to the boundary and stop there
        if (energyNormSquared <= 0) {

            field_type tau = positiveRoot(correctionNormSquared,
                                          2*solutionTimesCorrection,
                                          solutionNormSquared - trustRegionRadius_*trustRegionRadius_);

            // add scaled correction to current iterate
            x_->axpy(tau, p);

            std::cout << "Abort1, radius = " << std::sqrt(trustRegionScalarProduct(*x_,*x_)) << std::endl;

            if (this->historyBuffer_!="")
                this->writeIterate(*x_, i);
            return;
        }


        // minimize in given search direction p
        matrix_->mv(p,q);             // q=Ap
        field_type alpha = rholast/(p*q);       // scalar product

        // ////////////////////////////////////////////////////////////////////////
        //   Compute the new iterate.  If it is outside of the admissible set
        //   compute the point on the boundary where the search direction meets
        //   the boundary of the admissible set.
        // ////////////////////////////////////////////////////////////////////////

        VectorType tentativeNewIterate = *x_;
        tentativeNewIterate.axpy(alpha,p);           // update solution

        // Compute length of this tentative new iterate
        field_type tentativeLengthSquared = solutionNormSquared
            + 2*alpha*solutionTimesCorrection
            + alpha*alpha*correctionNormSquared;

//         std::cout << "tentative radius^2: " << trustRegionScalarProduct(tentativeNewIterate,tentativeNewIterate)
//                   << std::endl;
        //std::cout << "tentative length^2: " << tentativeLengthSquared << std::endl;

        if (tentativeLengthSquared >= trustRegionRadius_*trustRegionRadius_) {

            field_type tau = positiveRoot(correctionNormSquared,
                                          2*solutionTimesCorrection,
                                          solutionNormSquared - trustRegionRadius_*trustRegionRadius_);



            //*x_ = eta_j;  // return eta + tau*delta_j
            x_->axpy(tau, p);

            std::cout << "Abort2, radius = " << std::sqrt(trustRegionScalarProduct(*x_,*x_)) << std::endl;

            if (this->historyBuffer_!="")
                this->writeIterate(*x_, i);
            return;
        }

        *x_ = tentativeNewIterate;           // update solution
        rhs_->axpy(-alpha,q);          // update defect

        solutionNormSquared += 2*alpha*solutionTimesCorrection + alpha*alpha*correctionNormSquared;

        // determine new search direction
        q = 0;                      // clear correction

        // apply preconditioner
        if (preconditioner_) {
            preconditioner_->setProblem(*matrix_,q,b);
            preconditioner_->iterate();
            q = preconditioner_->getSol();
        } else
            q = b;

        rho = q*b;         // orthogonalization
        beta = rho/rholast;         // scaling factor
        p *= beta;                  // scale old search direction
        p += q;                     // orthogonalization with correction
        rholast = rho;              // remember rho for recurrence

        // recursion for the preconditioner norm of the correction
        solutionTimesCorrection = beta * ( solutionTimesCorrection + alpha*correctionNormSquared);

        correctionNormSquared = rholast  + beta*beta * correctionNormSquared;

        // //////////////////////////////////////////////////
        //   write iteration to file, if requested
        // //////////////////////////////////////////////////
        if (this->historyBuffer_!="")
            this->writeIterate(*x_, i);

        // //////////////////////////////////////////////////
        //   Compute error
        // //////////////////////////////////////////////////
        field_type oldNorm = (errorNorm_) ? errorNorm_->operator()(oldSolution) : oldSolution.two_norm();
        oldSolution -= *x_;

        field_type normOfCorrection = (errorNorm_) ? errorNorm_->operator()(oldSolution) : oldSolution.two_norm();

        if (this->useRelativeError_)
            error = normOfCorrection / oldNorm;
        else
            error = normOfCorrection;

        field_type convRate = normOfCorrection / normOfOldCorrection;

        normOfOldCorrection = normOfCorrection;

        if (!std::isinf(convRate) && !std::isnan(convRate)) {
            totalConvRate *= convRate;
            this->maxTotalConvRate_ = std::max(this->maxTotalConvRate_, std::pow(totalConvRate, 1/((double)convRateCounter+1)));
            convRateCounter++;
        }

        // Output
        if (this->verbosity_ == NumProc::FULL) {
            std::cout << std::setw(5) << i;

            std::cout << std::setiosflags(std::ios::scientific);
            std::cout << std::setw(15) << std::setprecision(7) << error;
            std::cout << std::resetiosflags(std::ios::scientific);

            std::cout << std::setiosflags(std::ios::fixed);
            std::cout << std::setw(9) << std::setprecision(5) << convRate;
            std::cout << std::resetiosflags(std::ios::fixed);

            std::cout << std::endl;
        }

    }


    if (this->verbosity_ != NumProc::QUIET) {
      std::cout << "maxTotalConvRate: " << this->maxTotalConvRate_ << ",   "
                  << i << " iterations performed\n";
        std::cout << "--------------------\n";
    }

}
