// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_ITERATIONSTEPS_CGSTEP_HH
#define DUNE_SOLVERS_ITERATIONSTEPS_CGSTEP_HH

#include <dune/solvers/common/preconditioner.hh>
#include <dune/solvers/common/defaultbitvector.hh>
#include <dune/solvers/iterationsteps/lineariterationstep.hh>

namespace Dune {
    namespace Solvers {

        //! A conjugate gradient solver
        template <class MatrixType, class VectorType, class Ignore = DefaultBitVector_t<VectorType>>
        class CGStep : public LinearIterationStep<MatrixType,VectorType,Ignore>
        {
            using Base = LinearIterationStep<MatrixType,VectorType,Ignore>;
            using Preconditioner = Dune::Solvers::Preconditioner<MatrixType, VectorType, Ignore>;

        public:
            CGStep()
                : preconditioner_(nullptr)
            {}

            CGStep(const MatrixType& matrix,
                   VectorType& x,
                   const VectorType& rhs)
                : Base(matrix,x), p_(rhs.size()), r_(rhs),
                  preconditioner_(nullptr)
            {}

            CGStep(const MatrixType& matrix,
                   VectorType& x,
                   const VectorType& rhs,
                   Preconditioner& preconditioner)
                : Base(matrix,x), p_(rhs.size()), r_(rhs),
                  preconditioner_(&preconditioner)
            {}

            //! Set linear operator, solution and right hand side
            virtual void setProblem(const MatrixType& mat, VectorType& x, const VectorType& rhs) override
            {
                this->setMatrix(mat);
                setProblem(x);
                r_ = rhs;
            }

            void check() const override;
            void preprocess();
            void iterate();

            using Base::setProblem;

        private:
            VectorType p_; // search direction
            VectorType r_; // residual
            using Base::x_;
            double r_squared_old_;
            Preconditioner* preconditioner_;
        };


#include "cgstep.cc"
    }
}

#endif
