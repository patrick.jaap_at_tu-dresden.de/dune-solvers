// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=8 sw=2 sts=2:

#include <config.h>

#include <tuple>

#include <dune/solvers/solvers/criterion.hh>


Dune::Solvers::Criterion::Result
Dune::Solvers::Criterion::operator()() const
{
  return f_();
}

std::string
Dune::Solvers::Criterion::header() const
{
  return header_;
}

Dune::Solvers::Criterion
Dune::Solvers::operator| (const Criterion& c1, const Criterion& c2)
{
  return Criterion(
      [=]() {
        auto r1 = c1();
        auto r2 = c2();
        return std::make_tuple(std::get<0>(r1) or std::get<0>(r2), std::get<1>(r1).append(std::get<1>(r2)));
      },
      c1.header().append(c2.header()));
}

Dune::Solvers::Criterion
Dune::Solvers::operator& (const Criterion& c1, const Criterion& c2)
{
  return Criterion(
      [=]() {
        auto r1 = c1();
        auto r2 = c2();
        return std::make_tuple(std::get<0>(r1) and std::get<0>(r2), std::get<1>(r1).append(std::get<1>(r2)));
      },
      c1.header().append(c2.header()));
}

Dune::Solvers::Criterion
Dune::Solvers::operator~ (const Criterion& c)
{
  return Criterion(
      [=]() {
        auto r = c();
        return std::make_tuple(not(std::get<0>(r)), std::get<1>(r));
      },
      c.header());
}

