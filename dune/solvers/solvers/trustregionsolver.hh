// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_SOLVERS_SOLVERS_TRUST_REGION_SOLVER_HH
#define DUNE_SOLVERS_SOLVERS_TRUST_REGION_SOLVER_HH

#include <vector>

#include <dune/common/bitsetvector.hh>

#include <dune/solvers/common/boxconstraint.hh>
#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/solvers/loopsolver.hh>

/** \brief A standard Trust-region solver with infinity-norm for the trust-region constraints, to solve an unconstrained nonlinear (possibly nonconvex) minimization problem. 
 *
 *  The energy functional is provided by the ProblemType class. It has to provide the following methods:
 *
 *      void assembleQP(const VectorType& iterate)
 *      field_type energy(const VectorType& iterate)
 *      field_type modelDecrease(const VectorType& iterate)
 *
 *      and member: 
 *          MatrixType A_
 *          VectorType f_
 *
 *      The method 'assembleQP' should assemble the local quadratic approximation at 'iterate'
 *      The local model is to be given by the quadratic part A_ and the linear part f_
 *
 *      The method 'energy' should evaluate the nonlinear energy at 'iterate'.
 *
 *      The method 'modelDecrease' should compute the decrease in the model-energy. 
 *
 *  The inner solver that has to be provided should be able to solve non-convex box-constrained quadratic problems,
 *  if the nonlinear functional is non-convex itself.
 *
 *  \tparam ProblemType A class representing the energy functional
 *
 * */
template <class ProblemType, class VectorType, class MatrixType>
class TrustRegionSolver
    : public IterativeSolver<VectorType>
{
    const static int blocksize = VectorType::value_type::dimension;
    typedef IterativeSolver<VectorType> Base;
    typedef typename VectorType::field_type field_type;

public:

    TrustRegionSolver()
        : Base(0,100,NumProc::FULL), enlargeRadius_(2), shrinkRadius_(0.5),
          goodIteration_(0.9), badIteration_(0.01)
    {}

    TrustRegionSolver(field_type tolerance, int maxIterations, NumProc::VerbosityMode verbosity=NumProc::FULL)
        : Base(tolerance, maxIterations, verbosity), enlargeRadius_(2), shrinkRadius_(0.5),
          goodIteration_(0.9), badIteration_(0.01)
    {}

    TrustRegionSolver(const Dune::ParameterTree& trConfig)
        : Base(0,100,NumProc::FULL)
    {
        setupTrParameter(trConfig);
    }

    //! Setup the standard trust-region parameter
    void setupTrParameter(const Dune::ParameterTree& trConfig) {

        this->tolerance_     = trConfig.get<field_type>("tolerance");
        this->maxIterations_ = trConfig.get<int>("maxIterations");
        this->verbosity_     = trConfig.get("verbosity",NumProc::FULL);
        initialRadius_       = trConfig.get<field_type>("initialRadius");
        maxRadius_           = trConfig.get<field_type>("maxRadius");
        enlargeRadius_       = trConfig.get("enlargeRadius",2.0);
        shrinkRadius_        = trConfig.get("shrinkRadius",0.5);
        goodIteration_       = trConfig.get("goodIteration",0.9);
        badIteration_        = trConfig.get("badIteration",0.01);

    }


    /** \brief Set up the solver. */
    template <class Norm>
    void setup(const Dune::ParameterTree& trConfig,
               Norm&& errorNorm,
               ProblemType& problem, Solver& innerSolver) {

        setupTrParameter(trConfig);
        this->setErrorNorm(std::forward<Norm>(errorNorm));
        problem_ = Dune::stackobject_to_shared_ptr(problem);
        innerSolver_ = Dune::stackobject_to_shared_ptr(innerSolver);
    }

    //! Solve the problem.
    void solve();

    //! Set the initial iterate
    void setInitialIterate(const VectorType& x) {x_ = x;}

    VectorType getSol() const {return x_;}

protected:

    //! Adjust the trust-region obstacles
    void scaleTrustRegionRadius(std::vector<BoxConstraint<field_type,blocksize> >& obs, const field_type& scaling)
    {
        if (scaling*initialRadius_>maxRadius_)
            initialRadius_ = maxRadius_;
        else
            initialRadius_ *= scaling;

        for (size_t i=0; i<obs.size(); i++)
            for (int j=0; j<blocksize; j++)
                obs[i][j] = { -initialRadius_, initialRadius_ };
    }

    /** \brief The problem type. */
    std::shared_ptr<ProblemType> problem_;

    /** \brief The solution vector */
    VectorType x_;

    /** \brief The solver for the quadratic inner problems */
    std::shared_ptr<Solver> innerSolver_;

    /** \brief The initial trust-region radius in the maximum-norm */
    field_type initialRadius_;

    /** \brief The maximal trust-region radius in the maximum-norm */
    field_type maxRadius_;

    /** \brief If the iteration is very successfull we enlarge the radius by this factor.*/
    field_type enlargeRadius_;

    /** \brief If the iteration is unsuccessfull we shrink the radius by this factor.*/
    field_type shrinkRadius_;

    /** \brief If the ratio between predicted and achieved decrease is above this number, the iteration is very successful.*/
    field_type goodIteration_;

    /** \brief If the ratio between predicted and achieved decrease is lower than this number, the iteration is unsuccessful.*/
    field_type badIteration_;
};

#include "trustregionsolver.cc"

#endif
