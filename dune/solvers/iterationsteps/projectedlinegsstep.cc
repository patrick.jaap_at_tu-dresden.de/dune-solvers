// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <dune/istl/scaledidmatrix.hh>

#include <cmath>

#include <dune/solvers/iterationsteps/projectedblockgsstep.hh>
#include <dune/solvers/norms/energynorm.hh>

template<class MatrixType, class VectorType, class BitVectorType>
void ProjectedLineGSStep<MatrixType, VectorType, BitVectorType >::
solveLocalSystem(const Dune::BTDMatrix<typename MatrixType::block_type>& matrix,
                 VectorType& x,
                 const VectorType& rhs,
                 const std::vector<BoxConstraint<field_type,BlockSize> >& localObstacles) const
{
    typedef Dune::BTDMatrix<typename MatrixType::block_type> LocalMatrixType;

    // /////////////////////////////
    //   Set initial iterate
    // /////////////////////////////
    x = 0;


    // //////////////////////////////////////
    //   The TNNMG loop
    // //////////////////////////////////////

    // stupid:
    Dune::BitSetVector<BlockSize> hasObstacle(rhs.size(), true);
    // The nodes to ignore are already written into the matrix/rhs
    Dune::BitSetVector<1> ignoreNodes(rhs.size(), false);

    for (int iteration=0; iteration<10; iteration++) {

        VectorType oldX = x;

        // ///////////////////////////
        //   Nonlinear presmoothing
        // ///////////////////////////
        VectorType rhsCopy = rhs;  // need a non-const version of rhs

        ProjectedBlockGSStep<LocalMatrixType, VectorType> nonlinearSmootherStep(matrix, x, rhsCopy);

        nonlinearSmootherStep.setIgnore(ignoreNodes);
        nonlinearSmootherStep.hasObstacle_ = &hasObstacle;
        nonlinearSmootherStep.obstacles_   = &localObstacles;

        for (int i=0; i<3; i++)
            nonlinearSmootherStep.iterate();

        // Compute residual
        VectorType residual = rhs;
        matrix.mmv(x,residual);

        // ///////////////////////////
        //   Truncation
        // ///////////////////////////

        LocalMatrixType truncatedMatrix = matrix;
        const double eps = 1e-12;
        for (size_t i=0; i<truncatedMatrix.N(); i++) {

            // test whether node is critical (== at the obstacle)
            assert(BlockSize==1);  // too lazy to program this for general block sizes
            if (localObstacles[i].lower(0) - x[i][0] >= -eps
                || localObstacles[i].upper(0) - x[i][0] <= eps) {

                // left off-diagonal:
                if (i>0)
                    truncatedMatrix[i][i-1] = 0;

                // diagonal
                truncatedMatrix[i][i] = Dune::ScaledIdentityMatrix<double,BlockSize>(1);

                // right off-diagonal:
                if (i<truncatedMatrix.N()-1)
                    truncatedMatrix[i][i+1] = 0;

                // right hand side
                residual[i] = 0;

            }

        }

        // ///////////////////////////
        //   Linear correction
        // ///////////////////////////

        VectorType linearCorrection(residual.size());
        truncatedMatrix.solve(linearCorrection, residual);   // the direct linear solution algorithm

        // //////////////////////////////////////////////////////////
        //   Line search in the direction of the projected coarse
        //   grid correction to ensure monotonicity.
        // //////////////////////////////////////////////////////////

        // L2-projection of the correction onto the defect obstacle
        for (size_t i=0; i<linearCorrection.size(); i++) {

            for (int j=0; j<BlockSize; j++) {

                linearCorrection[i][j] = std::max(linearCorrection[i][j], localObstacles[i].lower(j) - x[i][j]);
                linearCorrection[i][j] = std::min(linearCorrection[i][j], localObstacles[i].upper(j) - x[i][j]);

            }

        }

        // Construct obstacles in the direction of the projected correction
        BoxConstraint<field_type,1> lineSearchObs;
        for (size_t i=0; i<linearCorrection.size(); i++) {

            for (int j=0; j<BlockSize; j++) {
                // fmin and fmax ignore NAN values
                if (linearCorrection[i][j] > 0) {
                    lineSearchObs.lower(0) = std::fmax(lineSearchObs.lower(0),
                                                       (localObstacles[i].lower(j)-x[i][j]) / linearCorrection[i][j]);
                    lineSearchObs.upper(0) = std::fmin(lineSearchObs.upper(0),
                                                       (localObstacles[i].upper(j)-x[i][j]) / linearCorrection[i][j]);
                }
                if (linearCorrection[i][j] < 0) {
                    lineSearchObs.lower(0) = std::fmax(lineSearchObs.lower(0),
                                                       (localObstacles[i].upper(j)-x[i][j]) / linearCorrection[i][j]);
                    lineSearchObs.upper(0) = std::fmin(lineSearchObs.upper(0),
                                                       (localObstacles[i].lower(j)-x[i][j]) / linearCorrection[i][j]);
                }
            }

        }

        // abort when the linear correction has a small norm
        field_type correctionNormSquared = EnergyNorm<LocalMatrixType,VectorType>::normSquared(linearCorrection,truncatedMatrix);

        // Line search
        field_type alpha = (residual*linearCorrection) / correctionNormSquared;
        alpha = lineSearchObs[0].projectIn(alpha);

        if (isnan(alpha))
            alpha = 1;

        // add scaled correction
        x.axpy(alpha,linearCorrection);

        // //////////////////////////////////////////////////////
        //  terminate the loop if relative correction norm
        //  drops below machine precision.
        // //////////////////////////////////////////////////////

        oldX -= x;

        field_type relativeCorrection = std::sqrt(EnergyNorm<LocalMatrixType,VectorType>::normSquared(oldX,matrix))
            / std::sqrt(EnergyNorm<LocalMatrixType,VectorType>::normSquared(x,matrix));

        if (relativeCorrection < 1e-12)
            return;

    };

}



template<class MatrixType, class VectorType, class BitVectorType>
void ProjectedLineGSStep<MatrixType, VectorType, BitVectorType>::iterate()
{

    // input of this method: x^(k) (not the permuted version of x^(k)!)

    const MatrixType& matrix = *this->mat_;

    int number_of_blocks = this->blockStructure_.size();

    // iterate over the blocks
    for (int b_num=0; b_num < number_of_blocks; b_num++ ) {

        const int current_block_size = this->blockStructure_[b_num].size();

        //! compute and save the residuals for the curent block:
        // determine the (permuted) residuals r[p(i)],..., r[p(i+current_block_size-1)]
        // this means that we determine the residuals for the current block
        VectorType permuted_r_i(current_block_size);
        for (int k=0; k<current_block_size; k++)
          {
           if ( this->ignore()[this->blockStructure_[b_num][k]][0])
               permuted_r_i[k] = 0.0;
           else
               this->residual( this->blockStructure_[b_num][k], permuted_r_i[k]); // get r[p(i+k)]
          }
        // permuted_r_i[k] is the residual for r[p(i+k)], which means the residual \tilde{r}[i+k]
        // note: calling the method 'residual' with the permuted index implies that there is no additional permutation required within 'residual'

        // permuted_v_i is for saving the correction for x[p(i)],..., x[p(i+current_block_size-1)]:
        VectorType permuted_v_i(current_block_size); // permuted_v_i[k] = v[p(i+k)]
        // (Later: x_now[p(i+k)] = x_now[p(i+k)] + v[p(i+k)] )

        // //////////////////////////////////////////////////////////////////////////////////////////////////
        // Copy the linear system for the current line/block into a tridiagonal matrix
        // //////////////////////////////////////////////////////////////////////////////////////////////////

        Dune::BTDMatrix<typename MatrixType::block_type> tridiagonalMatrix(current_block_size);

        for (int j=0; j<current_block_size; j++) {

            if ( this->ignore()[this->blockStructure_[b_num][j]][0] ) {

                // left off-diagonal:
                if (j>0)
                    tridiagonalMatrix[j][j-1] = 0;

                // diagonal
                tridiagonalMatrix[j][j] = Dune::ScaledIdentityMatrix<field_type,BlockSize>(1);

                // left off-diagonal:
                if (j<current_block_size-1)
                    tridiagonalMatrix[j][j+1] = 0;

            } else {

                // left off-diagonal:
                if (j>0)
                    tridiagonalMatrix[j][j-1] = matrix[this->blockStructure_[b_num][j]][this->blockStructure_[b_num][j-1]];

                // diagonal
                tridiagonalMatrix[j][j] = matrix[this->blockStructure_[b_num][j]][this->blockStructure_[b_num][j]];

                // left off-diagonal:
                if (j<current_block_size-1)
                    tridiagonalMatrix[j][j+1] = matrix[this->blockStructure_[b_num][j]][this->blockStructure_[b_num][j+1]];

            }

        }

        // ///////////////////////////////////////////////////////////////////
        //   Get the local set of obstacles for this block
        // ///////////////////////////////////////////////////////////////////

        std::vector<BoxConstraint<field_type,BlockSize> > localDefectConstraints(current_block_size);

        for (int j=0; j<current_block_size; j++) {
            localDefectConstraints[j] = (*obstacles_)[this->blockStructure_[b_num][j]];
            localDefectConstraints[j] -= (*this->x_)[this->blockStructure_[b_num][j]];
        }

        // ////////////////////////////////////////
        //   Solve the tridiagonal system
        // ////////////////////////////////////////
        this->solveLocalSystem(tridiagonalMatrix, permuted_v_i, permuted_r_i, localDefectConstraints);

        // //////////////////////////////////////////////////////////////////////
        //   Add the correction to the current iterate
        // //////////////////////////////////////////////////////////////////////
       for (int k=0; k<current_block_size; k++)
           (*this->x_)[this->blockStructure_[b_num][k]] += permuted_v_i[k];

    }

}
