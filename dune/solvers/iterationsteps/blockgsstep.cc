// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <cassert>

#include <dune/matrix-vector/axpy.hh>

template<class MatrixType, class DiscFuncType, class BitVectorType>
inline
void BlockGSStep<MatrixType, DiscFuncType, BitVectorType>::
residual(int index, VectorBlock& r) const
{
    r = (*this->rhs_)[index];

    const auto& row = (*this->mat_)[index];
    for (auto cIt = row.begin(); cIt!=row.end(); ++cIt) {
        // r_i -= A_ij x_j
        Dune::MatrixVector::subtractProduct(r, *cIt, (*this->x_)[cIt.index()]);
    }

}

template<class MatrixType, class DiscFuncType, class BitVectorType>
inline
void BlockGSStep<MatrixType, DiscFuncType, BitVectorType>::iterate()
{
    assert(this->hasIgnore());

    if (gs_type_ != Direction::BACKWARD)
        for (std::size_t i=0; i<this->x_->size(); i++)
            iterate_step(i);

    if (gs_type_ != Direction::FORWARD)
        for (std::size_t i=this->x_->size()-1; i>=0 && i<this->x_->size(); i--)
            iterate_step(i);
}

template<class MatrixType, class DiscFuncType, class BitVectorType>
inline
void BlockGSStep<MatrixType, DiscFuncType, BitVectorType>::iterate_step(int i)
{
    auto const &ignore_i = this->ignore()[i];
    if (ignore_i.all())
        return;

    VectorBlock r;
    residual(i, r);
    const auto& mat_ii = (*this->mat_)[i][i];

    // Compute correction v = A_{i,i}^{-1} r[i]
    VectorBlock v;

    if (ignore_i.none()) {
        // No degree of freedom shall be ignored --> solve linear problem
        mat_ii.solve(v, r);
    } else {
        // Copy the matrix and adjust rhs and matrix so that dofs given in ignoreNodes[i]
        // are not touched
        typename MatrixType::block_type matRes;
        for (int j = 0; j < BlockSize; ++j) {
            if (ignore_i[j]) {
                r[j] = 0;

                for (int k = 0; k < BlockSize; ++k)
                    matRes[j][k] = (k == j);
            } else
                matRes[j] = mat_ii[j];

        }
        matRes.solve(v, r);
    }
    // Add correction
    VectorBlock& x = (*this->x_)[i];
    x += v;
}
