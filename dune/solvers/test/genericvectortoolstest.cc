// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <config.h>

#include <cmath>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/istl/bvector.hh>

#include <dune/solvers/common/genericvectortools.hh>


// This tests the interlace/deinterlace methods for consistency, i.e. if (x==deinterlace(interlace(x)))
template <class VectorType, class LargeVectorType, size_t vec_size, size_t inner_size>
bool check()
{

    bool passed = true;
    static const size_t block_size = VectorType::block_type::block_type::dimension;

    VectorType x(vec_size), x_re(vec_size);
    LargeVectorType y(inner_size);

    for (size_t i = 0; i<vec_size; ++i)
    {
        x[i].resize(inner_size);
        for (size_t j=0; j<inner_size; ++j)
            for (size_t k=0; k<block_size; ++k)
                x[i][j][k] = (1.0*rand()) / RAND_MAX;
    }

    GenericVector::interlace(x, y);
    GenericVector::deinterlace(y, x_re);

    x_re -= x;

    if (x_re.two_norm()>1e-12)
    {
        std::cout << "Test failure: deinterlace(interlace(x)) != x." << std::endl;
        passed = false;
    }

//    if (passed)
//        std::cout << "passed";
//    else
//        std::cout << "failed.";
//    std::cout << std::endl;

    return passed;
}

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);
    bool passed(true);

    static const size_t vec_size = 7;
    static const size_t inner_size = 4;

    typedef Dune::FieldVector<double,2> FV2;
    typedef Dune::FieldVector<double,3> FV3;
    typedef Dune::FieldVector<double,7> FV7;
    typedef Dune::FieldVector<double,15> FV11;

    passed =            check<Dune::BlockVector<Dune::BlockVector<FV2> >, Dune::BlockVector<Dune::FieldVector<double, vec_size*FV2::dimension> > , vec_size, inner_size>();
    passed = passed and check<Dune::BlockVector<Dune::BlockVector<FV3> >, Dune::BlockVector<Dune::FieldVector<double, vec_size*FV3::dimension> > , vec_size, inner_size>();
    passed = passed and check<Dune::BlockVector<Dune::BlockVector<FV7> >, Dune::BlockVector<Dune::FieldVector<double, vec_size*FV7::dimension> > , vec_size, inner_size>();
    passed = passed and check<Dune::BlockVector<Dune::BlockVector<FV11> >, Dune::BlockVector<Dune::FieldVector<double, vec_size*FV11::dimension> > , vec_size, inner_size>();

    return passed ? 0 : 1;
}
