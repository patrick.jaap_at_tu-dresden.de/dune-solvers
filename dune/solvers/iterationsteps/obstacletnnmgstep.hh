// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef OBSTACLE_TNNMG_STEP_HH
#define OBSTACLE_TNNMG_STEP_HH

// std includes
#include <array>
#include <vector>
#include <algorithm>

// dune-common includes
#include <dune/common/bitsetvector.hh>

// dune-solver includes
#include <dune/solvers/iterationsteps/iterationstep.hh>
#include <dune/solvers/iterationsteps/blockgssteps.hh>
#include <dune/solvers/iterationsteps/projectedblockgsstep.hh>
#include <dune/solvers/iterationsteps/multigridstep.hh>

#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/common/boxconstraint.hh>
#include <dune/solvers/transferoperators/mandelobsrestrictor.hh>





/**
 * \brief TNNMG step for quadratic obstacle problems
 *
 * This is a dune-solvers iteration step implementing
 * the TNNMG (truncated nonsmooth Newton multigrid) method
 * for convex quadratic obstacle problems for use in the
 * LoopSolver class.
 *
 * The method basically consists of a projected Gauss-Seidel
 * method as nonlinear pre- and post-smoother and a linear
 * multigrid method for a truncated linear system. For
 * details please refer to
 *
 *   'Multigrid Methods for Obstacle Problems', C. Gräser and R. Kornhuber
 *   Journal of Computational Mathematics 27 (1), 2009, pp. 1-44 
 *
 * A much more general and flexible implementation that can also be used for
 * energy functionals with nonquadratic (anisotropic) smooth part
 * and other nonsmooth part, e.g. incorporating local simplex
 * constraints, can be found in the dune-tnnmg module.
 *
 * \tparam MatrixType Type for the matrix describing the quadratic part
 * \tparam VectorType Type for solution and rhs vectors
 */
template<class MatrixType, class VectorType>
class ObstacleTNNMGStep
    : public IterationStep<VectorType>
{
        typedef IterationStep<VectorType> Base;
        typedef typename VectorType::field_type field_type;

        enum {blockSize = VectorType::block_type::dimension};
    public:

        typedef VectorType Vector;
        typedef MatrixType Matrix;
        typedef BoxConstraint<field_type, blockSize> BlockBoxConstraint;
        typedef typename std::vector<BlockBoxConstraint> BoxConstraintVector;
        typedef typename Dune::BitSetVector<VectorType::block_type::dimension> BitVector;

        typedef MultigridTransfer<Vector, BitVector, Matrix> Transfer;
        typedef typename std::vector<std::shared_ptr<Transfer>> TransferPointerVector;

    protected:

        typedef ProjectedBlockGSStep<Matrix, Vector> NonlinearSmoother;
        typedef Dune::Solvers::BlockGSStep<
        decltype(Dune::Solvers::BlockGS::LocalSolvers::gs()), Matrix, Vector>
            LinearSmoother;
        typedef MultigridStep<Matrix, Vector, BitVector> LinearMultigridStep;
        typedef EnergyNorm<Matrix, Vector> LinearBaseSolverEnergyNorm;
        typedef ::LoopSolver<Vector> LinearBaseSolver;

    public:

        /**
         * \brief Construct iteration step
         *
         * \param mat Matrix describing the quadratic part of the energy
         * \param x Solution vector containing the initial iterate
         * \param rhs Vector containing the linear part of the energy
         * \param ignore BitVector marking components to ignore (e.g. Dirichlet of hanging nodes)
         * \param obstacles std::vector containing local obstacles for each global index
         * \param transfer A vector of pointers to transfer operators describing the subspace hierarchy
         * \param truncationTol Components will be truncated for the coarse grid correction if they are
         *                      this close to the obstacle (the default will work well in general).
         *
         */
        ObstacleTNNMGStep(
                const Matrix& mat,
                Vector& x,
                const Vector& rhs,
                const BitVector& ignore,
                const BoxConstraintVector& obstacles,
                const TransferPointerVector& transfer,
                double truncationTol=1e-10) :
            Base(x),
            mat_(mat),
            rhs_(rhs),
            obstacles_(obstacles),
            transfer_(transfer),
            nonlinearSmoother_(mat_, *x_, rhs_),
            truncationTol_(truncationTol),
            linearSmoother_(Dune::Solvers::BlockGS::LocalSolvers::gs()),
            baseSolverStep_(Dune::Solvers::BlockGS::LocalSolvers::gs()),
            baseSolverNorm_(baseSolverStep_),
            baseSolver_(baseSolverStep_, 100, 1e-8, baseSolverNorm_, LinearBaseSolver::QUIET),
            preSmoothingSteps_(3),
            postSmoothingSteps_(3),
            linearIterationSteps_(1),
            linearPreSmoothingSteps_(3),
            linearPostSmoothingSteps_(3)
        {
            this->setIgnore(ignore);
        }

        /**
         * \brief Set type of multigrid cycle
         *
         * If you don't call this method manually, 3 nonlinear pre- and post-smoothing
         * steps and a linear V(3,3)-cycle will be used.
         *
         * \param preSmoothingSteps Number of nonlinear pre-smoothing steps.
         * \param linearIterationSteps Number of linear multigrid steps for truncated problem
         * \param postSmoothingSteps Number of nonlinear post-smoothing steps.
         * \param linearPreSmoothingSteps Number of linear pre-smoothing steps during the linear multigrid V-cycle
         * \param linearPostSmoothingSteps Number of linear post-smoothing steps during the linear multigrid V-cycle
         */
        void setSmoothingSteps(int preSmoothingSteps, int linearIterationSteps, int postSmoothingSteps, int linearPreSmoothingSteps, int linearPostSmoothingSteps)
        {
            preSmoothingSteps_ = preSmoothingSteps;
            postSmoothingSteps_ = postSmoothingSteps;
            linearIterationSteps_ = linearIterationSteps;
            linearPreSmoothingSteps_ = linearPreSmoothingSteps;
            linearPostSmoothingSteps_ = linearPostSmoothingSteps;
        }


        /**
         * \brief Setup the iteration step
         *
         * This method must be called before the iterate method is called.
         */
        virtual void preprocess()
        {
            truncatedResidual_.resize(x_->size());
            residual_.resize(x_->size());
            coarseCorrection_.resize(x_->size());
            temp_.resize(x_->size());

            linearMGStep_.setTransferOperators(transfer_);
            linearMGStep_.setSmoother(&linearSmoother_);
            linearMGStep_.setBaseSolver(baseSolver_);
            linearMGStep_.setMGType(1, linearPreSmoothingSteps_, linearPostSmoothingSteps_);

            hasObstacle_.resize(x_->size());
            hasObstacle_.setAll();

            nonlinearSmoother_.obstacles_ = &obstacles_;
            nonlinearSmoother_.hasObstacle_ = &hasObstacle_;
            nonlinearSmoother_.ignoreNodes_ = ignoreNodes_;
            outStream_.str("");
            outStream_ << "      step size";
            for(int j=0; j<blockSize; ++j)
                outStream_ << "  trunc" << std::setw(2) << j;
        }


        /**
         * \brief Apply one iteration step
         *
         * You can query the current iterate using the getSol() method
         * afterwards. The preprocess() method must be called before this
         * one.
         */
        virtual void iterate()
        {
            // clear previous output data
            outStream_.str("");

            // use reference to simplify the following code
            Vector& x = *x_;

            // nonlinear pre-smoothing
            for(int i=0; i<preSmoothingSteps_; ++i)
                nonlinearSmoother_.iterate();
            x = nonlinearSmoother_.getSol();

            // compute residual
            residual_ = rhs_;
            mat_.mmv(x, residual_);

            // initialize matrix, residual, and correction vector
            // for linear coarse grid correction
            Matrix truncatedMat = mat_;
            truncatedResidual_ = residual_;
            coarseCorrection_ = 0;

            // mark indices for truncation and count truncated indices per component
            typename std::array<int, blockSize> truncationCount;
            truncationCount.fill(0);
            BitVector truncate(x.size(), false);
            for(size_t i=0; i<x.size(); ++i)
            {
                for (int j = 0; j < blockSize; ++j)
                {
                    if ((x[i][j]<=obstacles_[i].lower(j)+truncationTol_) or (x[i][j]>=obstacles_[i].upper(j)-truncationTol_))
                    {
                        truncate[i][j] = true;
                        ++truncationCount[j];
                    }
                }
            }

            // truncate matrix and residual
            typename Matrix::row_type::Iterator it;
            typename Matrix::row_type::Iterator end;
            for(size_t i=0; i<x.size(); ++i)
            {
                it = truncatedMat[i].begin();
                end = truncatedMat[i].end();
                for(; it!=end; ++it)
                {
                    int j = it.index();
                    typename Matrix::block_type& Aij = *it;
                    for (int ii = 0; ii < blockSize; ++ii)
                        for (int jj = 0; jj < blockSize; ++jj)
                            if (truncate[i][ii] or truncate[j][jj])
                                Aij[ii][jj] = 0;
                }
                for (int ii = 0; ii < blockSize; ++ii)
                    if (truncate[i][ii])
                        truncatedResidual_[i][ii] = 0;
            }

            // apply linear multigrid to approximatively solve the truncated linear system
            linearMGStep_.setProblem(truncatedMat, coarseCorrection_, truncatedResidual_);
            linearMGStep_.ignoreNodes_ = ignoreNodes_;
            linearMGStep_.preprocess();
            for(int i=0; i<linearIterationSteps_; ++i)
                linearMGStep_.iterate();
            coarseCorrection_ = linearMGStep_.getSol();

            // post process multigrid correction
            // There will be spurious entries in non-truncated
            // components since we did not truncate the transfer
            // operators. These can safely be removed.
            for(size_t i=0; i<x.size(); ++i)
                for (int ii = 0; ii < blockSize; ++ii)
                    if (truncate[i][ii])
                        coarseCorrection_[i][ii] = 0;

            // project correction into the defect domain
            // and compute directional constraints for line search
            BoxConstraint<field_type, 1> directionalConstraints;
            for(size_t i=0; i<x.size(); ++i)
            {
                // compute local defect constraints
                BlockBoxConstraint defectConstraint = obstacles_[i];
                defectConstraint -= x[i];

                // project correction into local defect constraints
                for (int j = 0; j < blockSize; ++j)
                    coarseCorrection_[i][j]
                        = defectConstraint[j].projectIn(coarseCorrection_[i][j]);

                for (int j = 0; j < blockSize; ++j)
                {
                    // This may be Inf and if we multiply with it, we may get NaN.
                    // That is not a problem, however, since the resulting constraints
                    // are passed through std::fmin/fmax and will then be ignored.
                    auto const inverseCoarseCorrection =
                        field_type(1) / coarseCorrection_[i][j];

                    // compute relative defect constraints
                    defectConstraint.lower(j) *= inverseCoarseCorrection;
                    defectConstraint.upper(j) *= inverseCoarseCorrection;

                    // orient relative defect constraints
                    if (defectConstraint.lower(j) > defectConstraint.upper(j))
                        std::swap(defectConstraint.lower(j), defectConstraint.upper(j));

                    // update directional constraints
                    directionalConstraints.lower(0) = std::fmax(directionalConstraints.lower(0), defectConstraint.lower(j));
                    directionalConstraints.upper(0) = std::fmin(directionalConstraints.upper(0), defectConstraint.upper(j));
                }
            }

            // compute damping parameter
            mat_.mv(coarseCorrection_, temp_);
            field_type damping = 0;
            field_type coarseCorrectionNorm2 = coarseCorrection_*temp_;
            if (coarseCorrectionNorm2 > 0)
                damping = (residual_*coarseCorrection_) / coarseCorrectionNorm2;
            else
                damping = 0;
            damping = directionalConstraints[0].projectIn(damping);

            // apply correction
            x.axpy(damping, coarseCorrection_);

            // nonlinear post-smoothing
            for(int i=0; i<postSmoothingSteps_; ++i)
                nonlinearSmoother_.iterate();
            x = nonlinearSmoother_.getSol();

            // fill stream with output data
            outStream_.setf(std::ios::scientific);
            outStream_ << std::setw(15) << damping;
            for(int j=0; j<blockSize; ++j)
                outStream_ << std::setw(9) << truncationCount[j];
        }


        /**
         * \brief Get output accumulated during last iterate() call.
         *
         * This method is used to report TNNMG-specific data to the
         * LoopSolver class during the iterative solution.
         */
        std::string getOutput() const
        {
            std::string s = outStream_.str();
            outStream_.str("");
            return s;
        }


        /**
         * \brief Compute initial iterate by nested iteration
         *
         * This method performs a purely algebraic variant of nested iteration.
         * The coarse matrix and rhs are obtained using the transfer operators.
         * Coarse obstacles are constructed using a local max/min over the
         * support of coarse basis functions as suggested by J. Mandel.
         * The ignore-marks are restricted such that a coarse dof
         * is ignored if it is associated to an ignored fine dof
         * in the sense that the corresponding transfer operators entry is 1.
         *
         * On each level a fixed number of v-cycles is performed.
         *
         * This method can be called before the preprocess() method.
         * It does not change the ObstacleTNNMGStep state despite
         * updating the solution vector.
         *
         * \param coarseIterationSteps Number of v-cycle performed on the corse levels.
         */
        void nestedIteration(int coarseIterationSteps=2)
        {
            int maxLevel = transfer_.size();
            if (maxLevel==0)
            {
                *x_ = 0.0;
                return;
            }

            std::vector<Matrix> coarseMatrix(maxLevel);
            std::vector<Vector> coarseSolution(maxLevel);
            std::vector<Vector> coarseRhs(maxLevel);
            std::vector<BitVector> coarseIgnore(maxLevel);
            std::vector<BoxConstraintVector> coarseObstacle(maxLevel);
            MandelObstacleRestrictor<Vector> obstacleRestrictor;
            BitVector critical;

            critical.resize(x_->size());
            critical.unsetAll();

            transfer_[maxLevel-1]->galerkinRestrictSetOccupation(mat_, coarseMatrix[maxLevel-1]);
            transfer_[maxLevel-1]->galerkinRestrict(mat_, coarseMatrix[maxLevel-1]);
            transfer_[maxLevel-1]->restrict(rhs_, coarseRhs[maxLevel-1]);
            transfer_[maxLevel-1]->restrictToFathers(this->ignore(), coarseIgnore[maxLevel-1]);
            obstacleRestrictor.restrict(obstacles_, coarseObstacle[maxLevel-1], hasObstacle_, hasObstacle_, *(transfer_[maxLevel-1]), critical);
            coarseSolution[maxLevel-1].resize(coarseMatrix[maxLevel-1].N());
            for (int i = maxLevel-2; i>=0; --i)
            {
                transfer_[i]->galerkinRestrictSetOccupation(coarseMatrix[i+1], coarseMatrix[i]);
                transfer_[i]->galerkinRestrict(coarseMatrix[i+1], coarseMatrix[i]);
                transfer_[i]->restrict(coarseRhs[i+1], coarseRhs[i]);
                transfer_[i]->restrictToFathers(coarseIgnore[i+1], coarseIgnore[i]);
                obstacleRestrictor.restrict(coarseObstacle[i+1], coarseObstacle[i], hasObstacle_, hasObstacle_, *(transfer_[i]), critical);
                coarseSolution[i].resize(coarseMatrix[i].N());
            }

            coarseSolution[0] = 0;
            for (int i = 0; i<maxLevel; ++i)
            {

                TransferPointerVector coarseTransfer(i);
                for (int j = 0; j < i; ++j)
                    coarseTransfer[j] = transfer_[j];

                ObstacleTNNMGStep coarseTNNMGStep(
                        coarseMatrix[i],
                        coarseSolution[i],
                        coarseRhs[i],
                        coarseIgnore[i],
                        coarseObstacle[i],
                        coarseTransfer,
                        truncationTol_);
                coarseTNNMGStep.preprocess();
                for (int j = 0; j < coarseIterationSteps; ++j)
                    coarseTNNMGStep.iterate();
                coarseSolution[i] = coarseTNNMGStep.getSol();

                if (i<maxLevel-1)
                    transfer_[i]->prolong(coarseSolution[i], coarseSolution[i+1]);
            }
            transfer_[maxLevel-1]->prolong(coarseSolution[maxLevel-1], *x_);
        }

    protected:

        // problem description given by the user
        const Matrix& mat_;
        using Base::x_;
        using Base::ignoreNodes_;
        const Vector& rhs_;
        const BoxConstraintVector& obstacles_;

        // transfer operators given by the user
        const TransferPointerVector& transfer_;

        // vectors needed during iteration
        Vector residual_;
        Vector temp_;
        Vector truncatedResidual_;
        Vector coarseCorrection_;

        // solver components
        NonlinearSmoother nonlinearSmoother_;
        Dune::BitSetVector<blockSize> hasObstacle_;
        double truncationTol_;

        LinearMultigridStep linearMGStep_;
        LinearSmoother linearSmoother_;
        LinearSmoother baseSolverStep_;
        LinearBaseSolverEnergyNorm baseSolverNorm_;
        LinearBaseSolver baseSolver_;

        int preSmoothingSteps_;
        int postSmoothingSteps_;
        int linearIterationSteps_;
        int linearPreSmoothingSteps_;
        int linearPostSmoothingSteps_;

        mutable std::ostringstream outStream_;
};








#endif
