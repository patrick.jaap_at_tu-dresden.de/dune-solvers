// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef BLOCK_NORM_HH
#define BLOCK_NORM_HH

#include <memory>
#include <vector>
#include <cmath>

#include "norm.hh"

//! A norm for blocks of vectors
template <class V>
class BlockNorm: public Norm<V>
{
    public:
        using VectorType = V;
        using Base = Norm<V>;

        /** \brief The type used for the result */
        using typename Base::field_type;

//        typedef typename std::vector<const Norm<typename VectorType::block_type>* > NormPointerVector;
        typedef std::vector< std::shared_ptr<const Norm<typename VectorType::block_type> > > NormPointerVector;

        BlockNorm(const NormPointerVector& norms) :
            norms_(norms)
        {}

        //! Compute the norm of the given vector
        field_type operator()(const VectorType &v) const override
        {
            field_type r = 0.0;
            for (int i=0; i<norms_.size(); ++i)
            {
                auto ri = (*norms_[i])(v[i]);
                r += ri*ri;
            }
            return std::sqrt(r);
        }

        //! Compute the norm of the difference of two vectors
        field_type diff(const VectorType &v1, const VectorType &v2) const override
        {
            field_type r = 0.0;
            for (int i=0; i<norms_.size(); ++i)
            {
                auto ri = (*norms_[i]).diff(v1[i], v2[i]);
                r += ri*ri;
            }
            return std::sqrt(r);
        }

    private:
        const NormPointerVector& norms_;
};

#endif
