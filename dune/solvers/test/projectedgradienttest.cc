#include <config.h>

#include <array>
#include <iostream>
#include <sstream>

// dune-common includes
#include <dune/common/bitsetvector.hh>
#include <dune/common/parallel/mpihelper.hh>

// dune-istl includes
#include <dune/istl/bcrsmatrix.hh>

// dune-grid includes
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

// dune-solver includes
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/iterationsteps/projectedgradientstep.hh>
#include <dune/solvers/test/common.hh>

template<class MatrixType, class VectorType, class BitVector>
bool solveObstacleProblemByProjectedGradientSolver(const MatrixType& mat, VectorType& x, const VectorType& rhs, const BitVector& ignore, int maxIterations=10000, double tolerance=1.0e-10)
{
    // create double obstacle constraints
    const int blockSize = VectorType::block_type::dimension;

    typedef std::vector<BoxConstraint<double, blockSize> > BoxConstraintVector;
    BoxConstraintVector boxConstraints(rhs.size());
    for (size_t i = 0; i < boxConstraints.size(); ++i)
        for (size_t j = 0; j < blockSize; ++j)
          boxConstraints[i][j] = {
            (ignore[i][j]==true) ? 0.0 : -1.0,
            (ignore[i][j]==true) ? 0.0 : +1.0
          };

    ProjectedGradientStep<MatrixType, VectorType> projGradStep(mat, x, rhs);
    projGradStep.obstacles_ = &boxConstraints;
    projGradStep.setIgnore(ignore);

    //projGrad.iterate();
    EnergyNorm<MatrixType,VectorType>* h1SemiNorm = new EnergyNorm<MatrixType,VectorType>(mat);

    ::LoopSolver<VectorType> solver (projGradStep,
                                    maxIterations,    // iterations
                                    tolerance,   // tolerance
                                    *h1SemiNorm,
                                    Solver::REDUCED);
    solver.solve();
    x = projGradStep.getSol();
    return solver.getResult().converged;
}





template <class GridType>
bool checkWithGrid(const GridType& grid, const std::string fileName, int maxIterations)
{
    bool passed = true;

    static const int dim = GridType::dimension;

    typedef typename Dune::FieldMatrix<double, 1, 1> MatrixBlock;
    typedef typename Dune::FieldVector<double, 1> VectorBlock;
    typedef typename Dune::BCRSMatrix<MatrixBlock> Matrix;
    typedef typename Dune::BlockVector<VectorBlock> Vector;
    typedef typename Dune::BitSetVector<1> BitVector;

    typedef typename GridType::LeafGridView GridView;
    typedef typename Dune::FieldVector<double, dim> DomainType;
    typedef typename Dune::FieldVector<double, 1> RangeType;


    const GridView gridView = grid.leafGridView();

    Matrix A;
    constructPQ1Pattern(gridView, A);
    A=0.0;
    assemblePQ1Stiffness(gridView, A);

    Vector rhs(A.N());
    rhs = 0;
    ConstantFunction<DomainType, RangeType> f(50);
    assemblePQ1RHS(gridView, rhs, f);

    Vector u(A.N());
    u = 0;

    BitVector ignore(A.N());
    ignore.unsetAll();
    markBoundaryDOFs(gridView, ignore);


    solveObstacleProblemByProjectedGradientSolver(A, u, rhs, ignore, maxIterations);

    if (fileName!="")
    {
        typename Dune::VTKWriter<GridView> vtkWriter(gridView);
        vtkWriter.addVertexData(u, "solution");
        vtkWriter.write(fileName);
    }

    return passed;
}


template <int dim>
bool checkWithYaspGrid(int refine, const std::string fileName, int maxIterations)
{
    bool passed = true;

    typedef Dune::YaspGrid<dim> GridType;

    typename Dune::FieldVector<typename GridType::ctype,dim> L(1.0);
    typename std::array<int,dim> s;
    for (int i=0; i<dim; i++)
        s[i] = 1;
    std::bitset<dim> periodic(false);

    GridType grid(L, s, periodic, 0);

    for (int i = 0; i < refine; ++i)
        grid.globalRefine(1);

    std::cout << "Testing with YaspGrid<" << dim << ">" << std::endl;
    std::cout << "Number of levels: " << (grid.maxLevel()+1) << std::endl;
    std::cout << "Number of leaf nodes: " << grid.leafGridView().size(dim) << std::endl;

    passed = passed and checkWithGrid(grid, fileName, maxIterations);


    return passed;
}




int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);
    bool passed(true);


    int refine1d = 7;
    int refine2d = 5;
    int refine3d = 4;

    int maxIter1D = 10000;
    int maxIter2D = 1000;
    int maxIter3D = 100;

    if (argc>1)
        std::istringstream(argv[1]) >> refine1d;
    if (argc>2)
        std::istringstream(argv[2]) >> refine2d;
    if (argc>3)
        std::istringstream(argv[3]) >> refine3d;

    passed = passed and checkWithYaspGrid<1>(refine1d, "projectedgradienttest_yasp_1d_solution", maxIter1D);
    passed = passed and checkWithYaspGrid<2>(refine2d, "projectedgradienttest_yasp_2d_solution", maxIter2D);
    passed = passed and checkWithYaspGrid<3>(refine3d, "projectedgradienttest_yasp_3d_solution", maxIter3D);

    return passed ? 0 : 1;
}
