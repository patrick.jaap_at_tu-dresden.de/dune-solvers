// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef LOWRANKOPERATOR_HH
#define LOWRANKOPERATOR_HH


/** \brief Class that behaves like a filled in square low-rank matrix defined via a factor \f$ M\in\mathbb R^{k\times n}, k<<n \f$
 *
 *  The full matrix is given by \f$ A=M^TM\f$
 *
 *  \tparam LRFactorType The (Matrix-) type of the defining factor M
 */
template <class LRFactorType>
class LowRankOperator
{
    public:
        //! export the type of the defining factor
        typedef LRFactorType LowRankFactorType;
        //! export block type
        typedef typename LRFactorType::block_type block_type;
        //! export field type
        typedef typename LRFactorType::field_type field_type;
        //! export size type
        typedef typename LRFactorType::size_type size_type;

        //! default constructor
        LowRankOperator():
            factor_allocated_internally_(true)
        {
            lowRankFactor_ = new LowRankFactorType;
        }

        //! constructor taking the defining factor as argument
        LowRankOperator(LowRankFactorType& lrFactor):
            lowRankFactor_(&lrFactor),
            factor_allocated_internally_(false)
        {}

        ~LowRankOperator()
        {
            if (factor_allocated_internally_)
                delete lowRankFactor_;
        }

        //! b += Ax
        template <class LVectorType, class RVectorType>
        void umv(const LVectorType& x, RVectorType& b) const
        {
            LVectorType temp(lowRankFactor_->N());
            lowRankFactor_->mv(x,temp);
            lowRankFactor_->umtv(temp,b);
        }

        //! b += alpha*Ax
        template <class LVectorType, class RVectorType>
        void usmv(const field_type& alpha, const LVectorType& x, RVectorType& b) const
        {
            LVectorType temp(lowRankFactor_->N());
            lowRankFactor_->mv(x,temp);
            lowRankFactor_->usmtv(alpha,temp,b);
        }

        //! b -= Ax
        template <class LVectorType, class RVectorType>
        void mmv(const LVectorType& x, RVectorType& b) const
        {
            LVectorType temp(lowRankFactor_->N());
            lowRankFactor_->mv(x,temp);
            lowRankFactor_->usmtv(-1.0,temp,b);
        }

        //! b = Ax
        template <class LVectorType, class RVectorType>
        void mv(const LVectorType& x, RVectorType& b) const
        {
            b = 0.0;
            umv(x,b);
        }

        //! returns number of rows
        const size_type N() const
        {
            return lowRankFactor_->M();
        }

        //! returns number of columns
        const size_type M() const
        {
            return lowRankFactor_->M();
        }

        //! returns the defining factor
        const LowRankFactorType& lowRankFactor() const
        {
            return *lowRankFactor_;
        }

        //! returns the defining factor
        LowRankFactorType& lowRankFactor()
        {
            return *lowRankFactor_;
        }
    private:
        //! the defining factor
        LowRankFactorType* lowRankFactor_;

        const bool factor_allocated_internally_;

};

#endif

