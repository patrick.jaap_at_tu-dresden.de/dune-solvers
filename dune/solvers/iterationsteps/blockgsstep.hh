// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_BLOCK_GAUSS_SEIDEL_STEP_HH
#define DUNE_BLOCK_GAUSS_SEIDEL_STEP_HH

#warning This header is deprecated. Please use Dune::Solvers::BlockGSStep from blockgssteps.hh instead of ::BlockGSStep. See also gssteptest.cc for usage examples.

#include <dune/common/bitsetvector.hh>

#include "lineariterationstep.hh"

/** \brief A linear Gauß-Seidel iteration step for convex problems which have a block-structure.
 *
 *  \tparam MatrixType The linear operator type
 *  \tparam DiscFuncType The block vector type of the right hand side and the iterates
 *  \tparam BitVectorType The type of the bit-vector specifying degrees of freedom that shall be ignored.
 *
 */
template<class MatrixType,
         class DiscFuncType,
         class BitVectorType = Dune::BitSetVector<DiscFuncType::block_type::dimension> >
         class BlockGSStep : public LinearIterationStep<MatrixType, DiscFuncType, BitVectorType>
    {

        using VectorBlock = typename DiscFuncType::block_type;

        enum {BlockSize = VectorBlock::dimension};

    public:
        enum Direction { FORWARD, BACKWARD, SYMMETRIC };

        //! Default constructor.
        BlockGSStep(Direction gs_type = FORWARD)
            : gs_type_(gs_type)
        {}

        //! Constructor with a linear problem
        BlockGSStep(const MatrixType& mat, DiscFuncType& x, const DiscFuncType& rhs, Direction gs_type = FORWARD)
            : LinearIterationStep<MatrixType,DiscFuncType>(mat, x, rhs), gs_type_(gs_type)
        {}

        //! Perform one iteration
        virtual void iterate();

        /** \brief Compute the residual of the current iterate of a (block) degree of freedom
         *
         *  \param index Global index of the (block) degree of freedom
         *  \param r Write residual in this vector
         */
        virtual void residual(int index, VectorBlock& r) const;

    protected:
        const Direction gs_type_;

        void iterate_step(int i);
    };


#include "blockgsstep.cc"

#endif
