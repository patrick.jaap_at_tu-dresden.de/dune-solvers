// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_LINE_GAUSS_SEIDEL_STEP_HH
#define DUNE_LINE_GAUSS_SEIDEL_STEP_HH

#include <dune/matrix-vector/axpy.hh>

#include <dune/common/bitsetvector.hh>

template<class MatrixType,
         class DiscFuncType,
         class BitVectorType = Dune::BitSetVector<DiscFuncType::block_type::dimension> >
         class LineGSStep : public LinearIterationStep<MatrixType, DiscFuncType, BitVectorType>
    {
        using Base = public LinearIterationStep<MatrixType, DiscFuncType, BitVectorType>;
        typedef typename DiscFuncType::block_type VectorBlock;

        enum {BlockSize = VectorBlock::dimension};

    protected:

        // Describes the ordering of the degrees of freedom in 1d blocks
        std::vector<std::vector<unsigned int> > blockStructure_;

    public:

        //! Default constructor.  Doesn't init anything
        LineGSStep() {}

        //! Constructor for usage of Permutation Manager
        LineGSStep( const std::vector<std::vector<unsigned int> >& blockStructure)
            : blockStructure_( blockStructure )
        {}

        //! Constructor with a linear problem
        LineGSStep(const MatrixType& mat, DiscFuncType& x, DiscFuncType& rhs)
            : Base(mat, x, rhs)
        {}

        //! Perform one iteration
        virtual void iterate();

        /** \brief Compute the residual of the current iterate of a (block) degree of freedom
         *
         *  \param index Global index of the (block) degree of freedom
         *  \param r Write residual in this vector
         */
        virtual void residual(int index, VectorBlock& r) const {
            r = (*this->rhs_)[index];

            const auto& row = (*this->mat_)[index];
            for (auto cIt = row.begin(); cIt!=row.end(); ++cIt) {
                // r_i -= A_ij x_j
                Dune::MatrixVector::subtractProduct(r, *cIt, (*this->x_)[cIt.index()]);
            }
        }
    };


#include "linegsstep.cc"

#endif
